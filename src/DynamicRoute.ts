import { Elysia, t } from "elysia"
import { type Static } from "@sinclair/typebox"
import swagger from "@elysiajs/swagger"
export const methodSchema = t.Union([
  t.Literal("get"),
  t.Literal("post"),
  t.Literal("put"),
  t.Literal("patch"),
  t.Literal("delete"),
])
export type TMethod = Static<typeof methodSchema>
type TDynamicRouteOpts<T> = {
  group_path?: `/${string}`
  swaggerParams?: Parameters<typeof swagger>[number]
  name: T
  disableReload?: boolean
}
export const DynamicRoute =
  <const Name extends string = "jwt">(
    customApp = new Elysia(),
    opts: TDynamicRouteOpts<Name>
  ) =>
  (app: Elysia) =>
    app.decorate(opts.name, dynamicRoute(customApp, opts)(app))

export type TAddRoute = {
  method: TMethod
  path: string
  handler: (...args: any) => any
  hook?: object
}

function dynamicRoute<const Name extends string = "jwt">(
  customApp: Elysia,
  opts: TDynamicRouteOpts<Name>
) {
  return (app: Elysia) =>
    ({ method, path, handler, hook }: TAddRoute) => {
      const { group_path = `/${opts.name}`, disableReload } = opts
      customApp.group(group_path, (customApp) =>
        customApp.use(swagger(opts.swaggerParams)).use((customApp) => {
          if (method === "get") {
            customApp[method](path, handler, hook)
          } else if (method === "post") {
            customApp[method](path, handler, hook)
          } else if (method === "put") {
            customApp[method](path, handler, hook)
          } else if (method === "patch") {
            customApp[method](path, handler, hook)
          } else if (method === "delete") {
            customApp[method](path, handler, hook)
          } else {
            throw new Error(`${method} is not allow!!!`)
          }
          return customApp
        })
      )
      console.log(
        `info: ${group_path} ${method} ${path} has been added to the app. Please run method reload the app to see the changes.`
      )
      disableReload || reload(app, customApp)()
      return {
        reload: reload(app, customApp),
        detail: {
          method,
          group_path,
          path,
          fullPath: `${method} ${group_path}${path}`,
        },
      }
    }
}

function reload(app: Elysia, customApp: Elysia) {
  return () => {
    const port = app.server?.port!
    app.use(customApp)
    app.stop()
    app.listen(port)
    console.log(`info: app reloaded.`)
  }
}
