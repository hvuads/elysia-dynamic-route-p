import { Elysia, t } from "elysia"
import { DynamicRoute, methodSchema } from "./DynamicRoute"
import Stream from "@elysiajs/stream"
import swagger from "@elysiajs/swagger"
import { router, createContext } from "./trpc"
import cors from "@elysiajs/cors"
import { trpc } from "@elysiajs/trpc"
import "@elysiajs/trpc"
const port = +Bun.env.port! || 3000

const app = new Elysia()
  .use(cors())
  .use(swagger())
  .use(
    trpc(router, {
      createContext,
    })
  )
  .use(
    DynamicRoute(undefined, {
      name: "xxx",
      swaggerParams: {},
    })
  )
  .use(
    DynamicRoute(undefined, {
      name: "aaa",
      group_path: "/sss",
      swaggerParams: {},
    })
  )
  .get(
    "/",
    async () =>
      new Stream(async (stream) => {
        const x = setInterval(() => stream.send(Date.now()), 1000)
        setTimeout(() => {
          clearInterval(x)
          stream.close()
        }, 200_000)
      })
  )
  .post(
    "/",
    ({ body: { path, method }, xxx, aaa }) => {
      const conf = {
        method,
        path: `/${path}`,
        handler: ({ body: {} }) => ({ xxx: `/${path}` }),
        hook: {
          body: t.Object({
            path: t.String(),
            method: methodSchema,
          }),
        },
      }

      return [xxx(conf).detail, aaa(conf).detail]
    },
    {
      body: t.Object({
        path: t.String(),
        method: methodSchema,
      }),
    }
  )
  .post("/aa/xxxx/:id", () => ({ hcnd: 123 }), {
    query: t.Object({
      q: t.String(),
    }),
    body: t.Object({
      abc: t.Number(),
    }),
    params: t.Object({
      id: t.String(),
    }),
  })
  .listen(port)

export type AppTRPC = typeof app
console.log(
  `🦊 Elysia is running at http://${app.server?.hostname}:${app.server?.port}`
)
