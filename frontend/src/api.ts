/* eslint-disable */
import { edenTreaty } from "@elysiajs/eden"
import type { AppTRPC } from "@trpc"

const { origin, port } = window.location
const endpoint = origin.replace(port, "")

export const api = edenTreaty<AppTRPC>(
  `${endpoint}${import.meta.env.DEV ? "3000" : ""}`
)
