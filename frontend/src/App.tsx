import reactLogo from "./assets/react.svg"
import viteLogo from "/vite.svg"
import "./App.css"
import { api } from "./api"

function App() {
  async function send() {
    const data = await api.aa.xxxx["id"].post({
      abc: 1234,
      $query: { q: "ff" },
    })
    console.log("send  data:", data.data?.hcnd)
  }

  return (
    <>
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src={viteLogo} className="logo" alt="Vite logo" />
        </a>
        <a href="https://react.dev" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Vite + React</h1>
      <div className="card">
        <button onClick={send}>clicked</button>
      </div>
    </>
  )
}

export default App
